using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour
{
    public GameObject targetObject1;
    public GameObject targetObject2;
    public GameObject targetObject3;
    public GameObject targetObject4;
    public GameObject boxAlert;
    public GameObject lightSource;

    public bool weaponChoice;

    private void Start()
    {
        boxAlert.SetActive(false); //set weapon box alert inactive
      
    }

    public void OnTriggerEnter(Collider Col)
    {
        if (Col.gameObject.tag == "AlertArea")
        {
            boxAlert.SetActive(true); //if player is in range of weapon box activate alert
            weaponChoice = true; // if player is in range of weapon box set weapon choice to true
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "AlertArea")
        {
            boxAlert.SetActive(false); //if player is out of range of weapon box then the alert deactivates
            weaponChoice = false; //if player is out of range of weapon box then weapon choice is false
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L)) //if the L key is pressed do the following
        {
            if (lightSource.active)
            {
                lightSource.SetActive(false); //if the light on player torch is active and L is pressed then set it to false
            }

            else
            {
                lightSource.SetActive(true); //if the light on player torch is inactive and L is pressed then set it to true
            }
        }

        if (Input.GetKeyDown(KeyCode.G) && weaponChoice == true) //if the G key is pressed and weapon choice is true then do the following
        {
            float randomNumber = Random.Range(1, 5); //generate a random number

            if (randomNumber == 1)
            {
                targetObject1.SetActive(true); //if random number generated is 1 then set the corresponding weapon on player to active
                targetObject2.SetActive(false);
                targetObject3.SetActive(false);
                targetObject4.SetActive(false);
            }
            if (randomNumber == 2)
            {
                targetObject2.SetActive(true); //if random number generated is 2 then set the corresponding weapon on player to active
                targetObject1.SetActive(false);
                targetObject3.SetActive(false);
                targetObject4.SetActive(false);
            }
            if (randomNumber == 3)
            {
                targetObject3.SetActive(true); //if random number generated is 3 then set the corresponding weapon on player to active
                targetObject1.SetActive(false);
                targetObject2.SetActive(false);
                targetObject4.SetActive(false);
            }
            if (randomNumber == 4)
            {
                targetObject4.SetActive(true); //if random number generated is 4 then set the corresponding weapon on player to active
                targetObject1.SetActive(false);
                targetObject2.SetActive(false);
                targetObject3.SetActive(false);
            } 
        }
    }
}