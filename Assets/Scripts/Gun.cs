using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public Transform bulletSpawnPoint;
    public GameObject bulletPrefab;
    public AudioClip gunNoise;
    public float bulletSpeed = 10;
    public GameObject gunChoice;
    bool reloading = false;
    bool canShoot = true;
    float reloadTimer = 1f;
    public GameObject reloadingText;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F) && canShoot)
        {
            if (gunChoice.activeSelf)
            {
                for (int i = 0; i < 3; i++)
                {
                    AudioSource.PlayClipAtPoint(gunNoise, transform.position, 1);
                    var bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
                    bullet.GetComponent<Rigidbody>().velocity = bulletSpawnPoint.forward * bulletSpeed;
                    StartCoroutine(Reload());
                }
            }
            else
            {
                
                    AudioSource.PlayClipAtPoint(gunNoise, transform.position, 1);
                    var bullet = Instantiate(bulletPrefab, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
                    bullet.GetComponent<Rigidbody>().velocity = bulletSpawnPoint.forward * bulletSpeed;
                    StartCoroutine(Reload());
            }
        }
    }

    IEnumerator Reload()
    {
        reloading = true;
        canShoot = false;
        reloadingText.SetActive(true);

        yield return new WaitForSeconds(reloadTimer);
        //3 seconds wait after shooting a gun

        reloadingText.SetActive(false);
        canShoot = true;
        reloading = false;
    }
}
