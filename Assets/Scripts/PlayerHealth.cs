using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public static int lives = 10;
    int font = 40;

    public void OnTriggerEnter(Collider Col)
    {
        if (Col.gameObject.tag == "enemy")
        {
            lives = lives - 1;
        }

        if (Col.gameObject.tag == "enemy2")
        {
            lives = lives - 1;
        }
        if (Col.gameObject.tag == "enemy3")
        {
            lives = lives - 1;
        }
    }

    void Update()
    {
        if (lives < 1)
        {   
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }

    private void OnGUI()
    {
        GUI.skin.label.fontSize = font;
        GUI.Label(new Rect(300, 10, 6000, 200), "Lives : " + lives + "     Press L to toggle light");
    }
}
