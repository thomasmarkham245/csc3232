using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyAttack : MonoBehaviour
{    
    public GameObject damageTaken;
    public AudioClip attackNoise;
    public Slider healthBar;
    public Slider healthBar2;
    public Slider healthBar3;

    void Start()
    {
        damageTaken.SetActive(false);
    }

    private void Update()
    {
        healthBar.value = Bullet.enemyHealth;
        healthBar2.value = Bullet.enemyHealth2;
        healthBar3.value = Bullet.enemyHealth3;
    }

    public void OnTriggerEnter(Collider Col)
    {
        if (Col.gameObject.tag == "Player")
        {
            damageTaken.SetActive(true);
            AudioSource.PlayClipAtPoint(attackNoise, transform.position, 1);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            damageTaken.SetActive(false);
        }
    }
}

