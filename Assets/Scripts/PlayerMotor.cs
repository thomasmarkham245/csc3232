using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMotor : MonoBehaviour
{
    private CharacterController controller;
    public CharacterController PlayerHeight;

    private Vector3 playerVelocity;
    public Vector3 offset;

    public float speed;
    public float gravity = -9.8f;
    public float jumpHeight = 1f;
    public float counter = 0f;
    public float normalHeight, crouchHeight;

    public Transform player;

    public bool isGrounded;
    public bool isCrouched;
    public bool inVent = false;

    public GameObject Gun;
    public GameObject gravityIncreased;

    public static bool sprintOn;

    
    void Start()
    {
        controller = GetComponent<CharacterController>(); //get character controller
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C)) 
        {
            isCrouched = true; //If C key is pressed set crouched bool to true
            PlayerHeight.height = crouchHeight; //If C key is pressed change the player height to crouched height
            speed = 2f; //If C key is pressed decrease the player speed

        }

        if (Input.GetKeyUp(KeyCode.C) && inVent == false)
        {
            isCrouched = false; //If C key is released set crouched bool to false
            PlayerHeight.height = normalHeight;  //If C key is pressed change the crouched height to player height
            player.position = player.position + offset;
            speed = 5f; //If C key is release increase the player speed to normal
        }

        isGrounded = controller.isGrounded;
        if (Input.GetKeyDown(KeyCode.LeftShift) && isGrounded && isCrouched == true)
        { 
            speed = 4f; //If Left Shift is pressed and player is grounded and crouched then speed is set to lower than normal
            sprintOn = false; //If Left Shift is pressed and player is grounded and crouched then sprintOn is false
        }

        if (Input.GetKeyDown(KeyCode.LeftShift) && isGrounded && isCrouched == false)
        {
           speed = 10f; //If Left Shift is pressed and player is grounded and not crouched then speed is increased to sprinting speed
           sprintOn = true; //If Left Shift is pressed and player is grounded and not crouched then sprintOn is false
        }

        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
          speed = 5f; //If Left Shift is released then the player speed is set back to normal walking speed
          sprintOn = false; //If Left Shift is released then sprintOn is false
        }  
    }

    public void ProcessMove(Vector2 input) //Newtonian physics
    {
        Vector3 moveDirection = Vector3.zero;
        moveDirection.x = input.x;
        moveDirection.z = input.y;
        controller.Move(transform.TransformDirection(moveDirection) * speed * Time.deltaTime);
        playerVelocity.y += gravity * Time.deltaTime;

        if (isGrounded && playerVelocity.y < 0)
            playerVelocity.y = -2f;
            controller.Move(playerVelocity * Time.deltaTime);
    }

    public void Jump()
    {
        if (isGrounded)
        {
            playerVelocity.y = Mathf.Sqrt(jumpHeight * -3.0f * gravity); //Newtonian physics
        }
    }

    public void OnTriggerEnter(Collider Col)
    {
        if (Col.gameObject.tag == "walltrigger")
        {
            jumpHeight = 3f; //If player enters wall trigger then jump height is increased to 3
            gravityIncreased.SetActive(true);  //If player enter wall trigger then gravity increased sign is set true   
        }

        if (Col.gameObject.tag == "walltriggerexit")
        {
            jumpHeight = 1f; //If player enters wall trigger then jump height is decreased to 1
            gravityIncreased.SetActive(false); //If player enter wall trigger exit then gravity increased sign is set false   
        }

        if (Col.gameObject.tag == "gameWin")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 2); //If player enters game win zone then the scene is changed to the winning screen
        }

        if (Col.gameObject.tag == "vent")
        {
            inVent = true; //If player enters a vent , inVent status is set to true
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "vent")
        {
            inVent = false; //If player leaves a vent , inVent status is set to false
        }
    }
}
