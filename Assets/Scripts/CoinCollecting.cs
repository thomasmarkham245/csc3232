using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinCollecting : MonoBehaviour
{
    public int keys = 0;
    int font = 40;

    public GameObject enemyActive;
    public GameObject enemy2Active;
    public GameObject enemy3Active;
    public GameObject doorOpen;
    public GameObject doorClosed;
    public GameObject enemiesReleased;
    public GameObject doorReleased;
    public GameObject exitOpen;
    public GameObject map;

    public AudioClip keyPickup;

    void Start()
    {
        enemiesReleased.SetActive(false);
        exitOpen.SetActive(false);
    }



    public void OnTriggerEnter(Collider Col)
    {
        if(Col.gameObject.tag == "key") 
        {
            keys = keys + 1; //If player enters a key then 1 key is added to key count
            Col.gameObject.SetActive(false); //If player enters a key then key object is set to false
            AudioSource.PlayClipAtPoint(keyPickup, transform.position, 1); //If player enters a key then picking up key audio is played          
        }

        if (Col.gameObject.tag == "map")
        {
            Col.gameObject.SetActive(false); //If player enters map then map object is set to false
            map.SetActive(true); ////If player enters map then map is set to active on screen 
        }
    }

    private void OnGUI()
    {
        GUI.skin.label.fontSize = font;
        GUI.Label(new Rect(50, 10, 600, 120), "Keys : " + keys + "/10");
    }

    void Update()
    {
        if (keys < 5)
        {
            enemyActive.SetActive(false);
            enemy2Active.SetActive(false);
            enemy3Active.SetActive(false);
        }

        if (keys >= 5)
        {
            enemyActive.SetActive(true);
            enemy2Active.SetActive(true);
            enemy3Active.SetActive(true);
        }

        if (keys > 9)
        {
            Destroy(GameObject.FindWithTag("exit"));
            exitOpen.SetActive(true);
        }

        if(keys == 5)
        {
            enemiesReleased.SetActive(true);
        }

        if (keys >= 6)
        {
            enemiesReleased.SetActive(false);
        }

        if (keys < 8)
        {
            doorClosed.SetActive(true);
            doorOpen.SetActive(false);
            doorReleased.SetActive(false);
        }

        if(keys == 8)
        {
            doorOpen.SetActive(true);
            doorClosed.SetActive(false);
            doorReleased.SetActive(true);
        }

        if (keys > 8)
        {
            doorReleased.SetActive(false);
        }
    }


}
