using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour
{
    void Update()
    {
        transform.Rotate(20f * Time.deltaTime, 0f, 0f, Space.Self);
    }
}
