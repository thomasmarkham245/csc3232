using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bullet : MonoBehaviour
{
    public static int enemyHealth = 100;
    public static int enemyHealth2 = 100;
    public static int enemyHealth3 = 100;
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("enemy"))        
        {
            enemyHealth = enemyHealth - 20; // if bullet collides with enemy then enemy health is reduced by 20
            Destroy(gameObject); // this destroys the bullet

            if(enemyHealth <= 0) // if the enemy health is less than or equal to 0 then the enemy is destroyed
            {
                Destroy(other.gameObject); // this destroys the enemy
            }
        }
        if (other.gameObject.CompareTag("enemy2")) 
        {
            enemyHealth2 = enemyHealth2 - 20; // if bullet collides with enemy then enemy health is reduced by 20
            Destroy(gameObject); // this destroys the bullet

            if (enemyHealth2 <= 0) // if the enemy health is less than or equal to 0 then the enemy is destroyed
            {
                Destroy(other.gameObject); // this destroys the enemy
            }
        }

        if (other.gameObject.CompareTag("enemy3")) 
        {
            enemyHealth3 = enemyHealth3 - 20; // if bullet collides with enemy then enemy health is reduced by 20
            Destroy(gameObject); // this destroys the bullet

            if (enemyHealth3 <= 0) // if the enemy health is less than or equal to 0 then the enemy is destroyed
            {
                Destroy(other.gameObject); // this destroys the enemy
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("wall"))
        {
            Destroy(gameObject); // this destroys the bullet on collision with wall
        }

        if (collision.gameObject.CompareTag("bottle"))
        {
            PlayerHealth.lives = PlayerHealth.lives + 1; // if the bullet collides with a bottle then the player lives are increased by 1
        }
    }
}
   