using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyFollowing : MonoBehaviour
{
    
    public Transform playerObj;
    public Transform enemyObj;
    public Transform place;
    public Transform spawn;
    protected NavMeshAgent enemyMesh;
    float awareDistance = 20f;
    float attackDistance = 5f;
    private Animation anim;
    private bool pos = true;
    public AudioClip spottedNoise;
    public GameObject lightActive;

    void Start()
    {
        enemyMesh = GetComponent<NavMeshAgent>();
        anim = gameObject.GetComponent<Animation>();
    }

    //GOAP Attempt
    void Update()
    {
        float dist = Vector3.Distance(enemyObj.position, playerObj.position);
        if (dist < awareDistance && lightActive.activeSelf == true)
        {
            if (dist < attackDistance)
            {
                enemyMesh.SetDestination(playerObj.position);
                anim.Play("Attack1");
                GetComponent<NavMeshAgent>().speed = 1;
            }

            else
            {
                enemyMesh.SetDestination(playerObj.position);
                anim.Play("Run");
                GetComponent<NavMeshAgent>().speed = 4;
                if (dist < 10f)
                {
                    AudioSource.PlayClipAtPoint(spottedNoise, transform.position, 1);
                }
            }
        }

        else
        {   
            float dist2 = Vector3.Distance(enemyObj.position, place.position);
            float dist3 = Vector3.Distance(enemyObj.position, spawn.position);

            if (pos == true)
            {
                enemyMesh.SetDestination(place.position);
                anim.Play("Walk");
                GetComponent<NavMeshAgent>().speed = 2;

                if(dist2 < 4f)
                {
                    pos = false;
                }
            }

            if (pos == false) {
                enemyMesh.SetDestination(spawn.position);
                anim.Play("Walk");
                GetComponent<NavMeshAgent>().speed = 2;

                if(dist3 < 4f)
                {
                    pos = true;
                }
            }                
        }
    }
}

